# Maintainer: Jan de Groot <jgc@archlinux.org>

pkgname=upower-mobile
pkgver=0.99.11
pkgrel=3
pkgdesc="Abstraction for enumerating power devices, listening to device events and querying history and statistics"
url="https://upower.freedesktop.org"
arch=(x86_64 armv7h aarch64)
license=(GPL)
provides=("upower=$pkgver")
conflicts=("upower")
depends=(systemd libusb libimobiledevice libgudev)
makedepends=(docbook-xsl gobject-introspection python git gtk-doc)
backup=(etc/UPower/UPower.conf)
_commit=e1548bba61206a05bbc318b3d49ae24571755ac6  # tags/UPOWER_0_99_11^0
source=("git+https://gitlab.freedesktop.org/upower/upower.git#commit=$_commit"
        0001-Add-torch-support.patch
	0002-Detect-USB-Type-C-port-controller-and-other-chargers.patch
	0003-torch-be-more-specific-when-searching-for-an-led-dev.patch
	0004-daemon-Sync-icon-and-warning-for-non-default-low-level.patch
        UPower.conf)
md5sums=('SKIP'
         '84333de83863632ef09a8ba021ecd870'
         '2d8366707b9b513d0d5ba0413fc82625'
         '90e1dc62152517290d830590649061ca'
         'a00df797f68e128c40dd075a87f6d738'
         'ff6e13382557cb9a4d650de58187cf95')

pkgver() {
  cd upower
  git describe --tags | sed -e 's/UPOWER_//' -e 's/_/\./g' -e 's/-/+/g'
}

prepare() {
  cd upower

  local src
  for src in "${source[@]}"; do
      src="${src%%::*}"
      src="${src##*/}"
      [[ $src = *.patch ]] || continue
      echo "Applying patch $src..."
      patch -Np1 < "../$src"
  done

  sed -e 's|libplist >= 0.12|libplist-2.0 >= 2.2|' -i configure.ac # support libplist 2.2
  NOCONFIGURE=1 ./autogen.sh
}

build() {
  cd upower
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --libexecdir=/usr/lib \
    --disable-static \
    --enable-gtk-doc
  make
}

package() {
  cd upower
  make DESTDIR="$pkgdir" install
  # Shutdown when PMIC reports critical battery level
  pwd
  ls -lasih .
  ls -lasih ..
  install -Dm644 ../UPower.conf "$pkgdir"/etc/UPower/UPower.conf  
}
